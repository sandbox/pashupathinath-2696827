About "Bullion News" 
=======================

Bullion News module is a simple module which will add a block to your Drupal 7 web installtion 
to display the Indian Bullion News powered by Commodity Online portal.

Module Installation
======================

After activating the module (at "admin/modules") you can place the block on
any region on your web page (at "admin/structure/block").

Configurating the Module
==========================

The configuration is availabe after you added the block to your site. The
easiest way is to click the configure link next to the block. 

You can configure two aspects of the block:

1. Width

The width of the widget in pixels. The minimium value shoule be 210.

The variable used to store the value is "bullion_news_width"

2. Height

The width of the widget in pixels. The minimium value shoule be 175.

The variable used to store the value is "bullion_news_height"
